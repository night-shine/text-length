
const onUpdate = () => {
    let textLen = (document.querySelector("#text").value).length
    $("#preview").text(`preview Len: ${textLen}`)
}

const getLen = () => {
    let textElem = document.querySelector("#text").value
    let textLen = textElem.length
    $("#blank-div").clone().appendTo(".results")
    .html(`
        <div class="text-form">
            ${textElem}
        </div>
        <div class="len-form">
            ${textLen}
        </div> `
        )
}